from datetime import datetime
from pyspark.sql import SparkSession
from dataclasses import dataclass, field
import tinderbox as tb
from tinderbox.tinder import MaxValue, CompressRows



edge_case = {"data": [
        (1, datetime(2022, 2, 15)),
        (2, datetime(2022, 2, 15)),
        (2, datetime(2022, 2, 14)),
        (3, datetime(2022, 2, 14))
    ],
        "schema": ['id', 'run_date']}


@dataclass
class SampleModel(object):
    run_date: datetime = field()

    @property
    def id(self):
        pass 

def example():
    # Make a TinderBox transformer
    transformer = tb.make(df_model=SampleModel)

    # Gonna be referenced a few times.
    rd_col = transformer.df.run_date

    # need to add a global value for comparisons, This is lazy loaded so it won't be called until
    # it is needed. to force this evaluatioin at a specfic time add it to the transform pipeline.
    transformer.max_run = MaxValue(rd_col)

    # Remove Dupes keep the latest based on run_date
    transformer.transform(CompressRows(on=SampleModel.id, order=rd_col.desc()),
                               tb.AddColumn("active", transformer.max_run == rd_col))

    return transformer

def get_spark():
    return SparkSession.builder \
        .master("local") \
        .appName("Test") \
        .getOrCreate()

def test_compress():
    spark = get_spark()
    df = spark.createDataFrame(**edge_case)
    df2 = example().ignite(df)
    df2.show()