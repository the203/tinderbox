
TinderBox is a Library for PySpark that allows for the creation of a sequence of events to be created. That sequence of events can be shared / modified then when ready fed a dataframe and the sequence is executed. 

TinderBox provides an architecture to define transformation steps (DataFrameOperations) and provides a few transformations as well
